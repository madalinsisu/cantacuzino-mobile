import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { device } from "tns-core-modules/platform/platform";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    private code: string;
    constructor(private barcodeScanner: BarcodeScanner) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    showCode() {
        alert(this.code);
        alert(device.uuid);
    }

    public scan() {
        
        this.barcodeScanner.scan({
            cancelLabel: "Stop scanning",
            message: "Go scan something",
            preferFrontCamera: false,
            showFlipCameraButton: true
        }).then((result) => {
            this.code = result.text;
        }).catch(err => {
            alert("err");
        })
        
    }

    // public onScan() {

    //     this.barcodeScanner.scan({
    //         formats: "QR_CODE, EAN_13",
    //         showFlipCameraButton: true,   
    //         preferFrontCamera: false,     
    //         showTorchButton: true,        
    //         beepOnScan: true,             
    //         torchOn: false,               
    //         resultDisplayDuration: 500,   
    //         orientation: "vertical",     
    //         openSettingsIfPermissionWasPreviouslyDenied: true //ios only 
    //     }).then((result) => {
    //         alert({
    //             title: "You Scanned ",
    //             message: "Format: " + result.format + ",\nContent: " + result.text,
    //             okButtonText: "OK"
    //         });
    //         }, (errorMessage) => {
    //             alert(errorMessage);
    //             console.log("Error when scanning " + errorMessage);
    //         }
    //     );

    //     // this.barcodeScanner.scan({
    //     //     formats: "QR_CODE, EAN_13",
    //     //     showFlipCameraButton: true,   
    //     //     preferFrontCamera: false,     
    //     //     showTorchButton: true,        
    //     //     beepOnScan: true,             
    //     //     torchOn: false,               
    //     //     resultDisplayDuration: 500,   
    //     //     orientation: orientation,     
    //     //     openSettingsIfPermissionWasPreviouslyDenied: true //ios only 
    //     // }).then((result) => {
    //     //     alert({
    //     //         title: "You Scanned ",
    //     //         message: "Format: " + result.format + ",\nContent: " + result.text,
    //     //         okButtonText: "OK"
    //     //     });
    //     //     }, (errorMessage) => {
    //     //         console.log("Error when scanning " + errorMessage);
    //     //     }
    //     // );
    // }
}

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GalleryComponent } from "~/gallery/gallery.component";
import { GalleryRoutingModule } from "~/gallery/gallery-routing.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        GalleryRoutingModule
    ],
    declarations: [
        GalleryComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class GalleryModule { }